import threading
import unittest
import os
import time
import HTMLTestRunner
from appium import webdriver
import paramunittest

caps1 = {
	"platformName": "Android",
	"appium:platformVersion": "7.1.2",
	"appium:deviceName": "emulator-5554",
	"appium:appPackage": "cn.itcast.order",
	"appium:appActivity": "cn.itcast.order.activity.ShopActivity",
	"appium:ensureWebviewsHavePages": True,
	"appium:nativeWebScreenshot": True,
	"appium:newCommandTimeout": 0,
	"appium:connectHardwareKeyboard": True
}
caps2 = {
	"platformName": "Android",
	"appium:platformVersion": "11",
	"appium:deviceName": "emulator-5554",
	"appium:appPackage": "cn.itcast.order",
	"appium:appActivity": "cn.itcast.order.activity.ShopActivity",
	"appium:ensureWebviewsHavePages": True,
	"appium:nativeWebScreenshot": True,
	"appium:newCommandTimeout": 0,
	"appium:connectHardwareKeyboard": True
}




class Dttest(unittest.TestCase):

	caps={}

	@classmethod
	def setUpClass(cls):
		print('------start setup------')
		print(cls.caps)

		cls.driver = webdriver.Remote("http://127.0.0.1:4723/wd/hub", cls.caps)

	@classmethod
	def tearDownClass(cls):
		cls.driver.quit()
		print('------tearDown------')

	def test_sleep(self):  # 用例
		time.sleep(10)
		print('------sleep passed------')

	def test_failure(self):
		# 失败测试
		time.sleep(2)
		self.driver.find_element_by_id('login').click()
		time.sleep(2)
		print('------failure test------')

	def test_text(self):  # 用例
		time.sleep(3)
		text = self.driver.find_element_by_xpath(
			r'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.ListView/android.widget.RelativeLayout[1]/android.widget.LinearLayout[1]/android.widget.TextView[1]')
		print('获取的文本内容：', text.text)
		time.sleep(3)
		print('------text passed------')

	def test_click(self):  # 用例
		time.sleep(5)
		self.driver.find_element_by_xpath(
			r'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.ListView/android.widget.RelativeLayout[1]').click()
		time.sleep(3)
		self.driver.find_element_by_xpath(
			r'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout[2]/android.widget.ListView/android.widget.RelativeLayout[1]/android.widget.Button').click()
		time.sleep(3)
		self.driver.find_element_by_id(r'cn.itcast.order:id/tv_settle_accounts').click()
		time.sleep(3)
		self.driver.find_element_by_xpath(
			r'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.EditText').send_keys(
			r'辽宁工程技术大学葫芦岛校区')
		time.sleep(3)
		self.driver.find_element_by_id(r'cn.itcast.order:id/tv_payment').click()
		print('支付成功')
		print('------click passed------')



def test_body(caps, fileName, test_examples):
	"""
	:param caps: Desired Capabilities 测试参数
	:param fileName: 测试报告存储路径   '例：.../reports/report.html'
	:param test_examples: 测试用例名
	:return:
	"""
	Dttest.caps = caps
	suite = unittest.TestSuite()
	for example in test_examples:
		suite.addTest(Dttest(example))

	# timestr = time.strftime('%Y-%m-%d %X', time.localtime(time.time()))  # 本地日期时间作为测试报告的名字
	filename = fileName  # 这个路径改成自己的目录路径
	print('3')

	fp = open(filename, 'wb')
	runner = HTMLTestRunner.HTMLTestRunner(
		stream=fp,
		verbosity=2,
		title='result',
		description='report'
	)
	runner.run(suite)
	fp.close()


def test_android7():
	test_examples = ['test_text', 'test_failure', 'test_text', 'test_click']
	test_body(caps1, 'D:\\PyCharm编程\\WorkSpace\\机器学习实战\\appium\\report\\android7.html', test_examples)


def test_android11():
	test_examples = ['test_sleep', 'test_failure', 'test_text', 'test_click']
	test_body(caps2, 'D:\\PyCharm编程\\WorkSpace\\机器学习实战\\appium\\report\\android11.html', test_examples)


if __name__ == '__main__':
	# test_android7()
	test_android11()