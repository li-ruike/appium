import unittest
import os
import time
import HTMLTestRunner
from appium import webdriver



class Dttest(unittest.TestCase):
	@classmethod
	def setUpClass(cls):
		print('start setup')

		caps = {}
		caps["platformName"] = "Android"
		caps["appium:platformVersion"] = "7.1.2"
		caps["appium:deviceName"] = "emulator-5554"
		caps["appium:appPackage"] = "com.changmi.calculator"
		caps["appium:appActivity"] = "com.changmi.calculator.CalculatorNormal"
		caps["appium:ensureWebviewsHavePages"] = True
		caps["appium:nativeWebScreenshot"] = True
		caps["appium:newCommandTimeout"] = 0
		caps["appium:connectHardwareKeyboard"] = True

		cls.driver = webdriver.Remote("http://127.0.0.1:4723/wd/hub", caps)

	@classmethod
	def tearDownClass(cls):
		cls.driver.quit()
		print('tearDown')

	def test_sleep(self):  # 用例
		time.sleep(10)
		print('sleep passed')

	def test_clicktap1(self):  # 用例
		time.sleep(3)
		self.driver.find_element_by_name('爱物').click()
		time.sleep(3)
		print('click passed')

	def test_multiple(self):  # 用例
		time.sleep(5)
		self.driver.find_element_by_id('com.changmi.calculator:id/nine').click()
		time.sleep(1)
		self.driver.find_element_by_id('com.changmi.calculator:id/multiple').click()
		time.sleep(1)
		self.driver.find_element_by_id('com.changmi.calculator:id/six').click()
		time.sleep(1)
		self.driver.find_element_by_id('com.changmi.calculator:id/eq').click()
		print('click passed')


if __name__ == '__main__':

	suite = unittest.TestSuite()
	suite.addTest(Dttest('test_sleep'))  # 需要测试的用例就addTest，不加的就不会运行
	suite.addTest(Dttest('test_clicktap1'))
	suite.addTest(Dttest('test_multiple'))
	# unittest.TextTestRunner(verbosity=1).run(suite)
	timestr = time.strftime('%Y-%m-%d %X', time.localtime(time.time()))  # 本地日期时间作为测试报告的名字
	filename = 'D:\\PyCharm编程\\WorkSpace\\appium\\appium\\report\\repot.html'  # 这个路径改成自己的目录路径
	print('3')


	fp = open(filename, 'wb')
	runner = HTMLTestRunner.HTMLTestRunner(
		stream=fp,
		verbosity = 2,
		title='result',
		description='report'
	)
	runner.run(suite)
	fp.close()
