

from appium import webdriver
import time

caps = {}
caps["platformName"] = "Android"
caps["appium:platformVersion"] = "7.1.2"
caps["appium:deviceName"] = "emulator-5554"
caps["appium:appPackage"] = "com.changmi.calculator"
caps["appium:appActivity"] = "com.changmi.calculator.CalculatorNormal"
caps["appium:ensureWebviewsHavePages"] = True
caps["appium:nativeWebScreenshot"] = True
caps["appium:newCommandTimeout"] = 0
caps["appium:connectHardwareKeyboard"] = True

driver = webdriver.Remote("http://127.0.0.1:4723/wd/hub", caps)
time.sleep(5)


driver.find_element_by_id('com.changmi.calculator:id/nine').click()
time.sleep(1)
driver.find_element_by_id('com.changmi.calculator:id/multiple').click()
time.sleep(1)
driver.find_element_by_id('com.changmi.calculator:id/six').click()
time.sleep(1)
driver.find_element_by_id('com.changmi.calculator:id/eq').click()

# driver.quit()

